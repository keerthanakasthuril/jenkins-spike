# Jenkins-spike

Spike for jenkins setup

## Userdata for hosting Jenkins in EC2

```shell
#!/bin/bash
sudo apt update
sudo apt -y install openjdk-11-jdk
java -version
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > \
    /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update
sudo apt-get -y install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get -y install docker-ce docker-ce-cli containerd.io
sudo groupadd docker
sudo usermod -aG docker $USER
sudo apt-get -y install jenkins
sudo usermod -aG docker jenkins
service jenkins restart
```
## Security group for EC2
- 22 - SSH
- 80 - HTTP
- 8080 - Custom TCP

## IAM Roles attached
- AmazonEC2ContainerRegistryFullAccess
- AmazonS3FullAccess
- EC2InstanceProfileForImageBuilderECRContainerBuilds
- AmazonElasticContainerRegistryPublicFullAccess

## Plugins for Jenkins
- Bitbucket
- Bitbucket branch source
- Blue ocean
- Docker
- Docker pipeline

## Adding webhook to Bitbucket
Goto repository settings-> Webhooks -> Add webhook -> Create a webhook with URL `http://<ec2_public_ip>:8080/bitbucket-scmsource-hook/notify`

