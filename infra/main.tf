provider "aws" {
  region = "eu-central-1"
}

terraform {
  backend "s3" {
    bucket = "spike-terraform-backend"
    key    = "github-actions-spike"
    region = "eu-central-1"
  }
}

module "nginx_ecr" {
  source = "./ecr"
}
