FROM node:12.19.0-alpine3.12

RUN apk update && \
    apk add terraform --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community && \
    apk add zip

RUN apk add --no-cache \
        python3 \
        py3-pip \
    && pip3 install --upgrade pip \
    && pip3 install \
        awscli \
    && rm -rf /var/cache/apk/*

RUN apk add jq

RUN apk add --update docker openrc
RUN rc-update add docker boot
